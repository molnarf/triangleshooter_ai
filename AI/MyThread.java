package AI;

import java.util.List;

import PowerUps.PowerUp;

public class MyThread extends Thread{
		public List<AI_Game> games;
		public boolean add_powerup;
		public PowerUp p_up;
		public double time_passed;
		public boolean generation_finished = true;
		public String name;
		
		public MyThread(List<AI_Game> games, boolean add_powerup, PowerUp p_up, double time_passed, String name){
		   this.games = games;
		   this.add_powerup = add_powerup;
		   this.p_up = p_up;
		   this.time_passed = time_passed;
		   this.name = name;
		}
	
		public void run(){
			for(AI_Game game : games){
				if(add_powerup) {
					game.add_powerup(p_up);
				}
				game.update(time_passed);
				if(game.ingame){
					generation_finished = false;
				}
			}
		}
	}
