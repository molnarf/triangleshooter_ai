package AI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import Game.C;
import Game.Game;
import PowerUps.PowerUp;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Population {

	public ArrayList<AI> bots_left;
	public ArrayList<AI> bots_right;
	public ArrayList<AI_Game> games;
	public int population_size = 400;
	private int num_new_players = 0;
	private int num_offspring = population_size-num_new_players -1; 
	public int generation = 1;
	public int view_player_nr = 0;
	// -1 because the best players survive without mutation
	public boolean generation_finished = false;
	
	public Population(){
		this.bots_left = new ArrayList<>(population_size);
		this.bots_right = new ArrayList<>(population_size);
		this.games = new ArrayList<>();
		for(int i = 0; i<population_size; i++){
			create_new_ai_game_with_random_bots();
		}
	}
	
	/**
	 * Creates offspring by comparing the scores of the left and right population respectively
	 */
	public void createOffspring() {
		this.generation++;
		this.games.clear();
		Collections.sort(this.bots_right, new AI_Comparator());
		Collections.sort(this.bots_left, new AI_Comparator());
		print_results();

		ArrayList<AI> tmp_left = new ArrayList<>(population_size);
		ArrayList<AI> tmp_right = new ArrayList<>(population_size);
		
		reset_best_players(bots_left.get(0), bots_right.get(0), tmp_left, tmp_right);
		for(int i = 0; i<num_offspring; i++) {
			if(i<num_offspring/2) {
				AI AI_left = new AI(get_parent(true));
				AI AI_right = new AI(get_parent(false));
				add_offspring_to_lists(AI_left, AI_right, tmp_left, tmp_right);
			}
			else { // crossover
				AI Jesus_left = get_parent(true).crossover(get_parent(true));
				AI Jesus_right = get_parent(false).crossover(get_parent(false));
				add_offspring_to_lists(Jesus_left, Jesus_right, tmp_left, tmp_right);
			}
		}
		this.bots_left = tmp_left;
		this.bots_right = tmp_right;
		mutate_players();
		
		for(int i = 0; i<num_new_players; i++) {
			this.create_new_ai_game_with_random_bots();
		}
	}
	
	public void add_offspring_to_lists(AI left_AI, AI right_AI, ArrayList<AI> tmp_left, ArrayList<AI> tmp_right){
		tmp_left.add(left_AI);
		tmp_right.add(right_AI);
		this.games.add(new AI_Game(left_AI, right_AI));
	}
	
	
	// mutate all players except the first one
	public void mutate_players() {
		for(int i = 1; i<this.bots_left.size(); i++) {
			bots_left.get(i).mutate2();
			bots_right.get(i).mutate2();
		}
	}
	
	public void reset_best_players(AI best_left, AI best_right, ArrayList<AI> tmp_left, ArrayList<AI> tmp_right) {
		System.out.println("BEST SCORE LEFT: " + best_left.score);
		System.out.println("BEST SCORE RIGHT: " + best_right.score);
		best_left.best_player = true;
		best_right.best_player = true;
		best_left.init(200, 200, true, Color.GREEN);
		best_left.init_media();
		best_right.init(600, 400, false, Color.RED);
		best_right.init_media();
		this.games.add(new AI_Game(best_left, best_right));
		tmp_left.add(best_left);
		tmp_right.add(best_right);
	}
	
	public void create_new_ai_game_with_random_bots() {
		AI left_player = new AI(200, 400, true, Color.GREEN);
		AI right_player = new AI(600, 400, false, Color.RED);
		left_player.init_weights();
		right_player.init_weights();
		this.bots_left.add(left_player);
		this.bots_right.add(right_player);
		this.games.add(new AI_Game(left_player, right_player));
	}
	
	
	public void render(GraphicsContext gc){
		this.games.get(this.view_player_nr).render(gc);
	}
	
	
	public void update(double time_passed){
		Random rd = new Random();
		generation_finished = true;
		PowerUp p_up = null;
		boolean add_powerup = false;
		if(rd.nextInt(1000) < 20 * C.game_speed){
			p_up = Game.get_random_powerup();
			add_powerup = true;
		}
		double num_threads = 4;
		ArrayList<MyThread> threads = new ArrayList<>();
		for(int i = 0; i<num_threads; i++) {
			List<AI_Game> games_portion = games.subList((int)((i/num_threads) * games.size()), (int)(((i+1)/num_threads) * games.size()));
			MyThread t = new MyThread(games_portion, add_powerup, p_up, time_passed, Integer.toString(i));
			threads.add(t);
			t.start();
		}
		for(MyThread t: threads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		for(MyThread t: threads) {
			if(!t.generation_finished) {
				generation_finished = false;
			}
		}
	}
	
	// returns a parent with a probability proportional to its score
	public AI get_parent(boolean left) {
		int sum = 0;
		ArrayList<AI> bots = this.bots_right;
		Random r = new Random();
		if(left) {
			bots = this.bots_left;
		}
		for(AI bot: bots) {
			sum += bot.score; 
		}
		
		if(sum != 0) {
			int runningSum = 0;
			int randomValue = r.nextInt(sum); 
			for(AI bot: bots) {
				runningSum += bot.score;
				if(runningSum >= randomValue) {
					return bot;
				}
			}
		}
		// if all players have a score of 0, return a new random ai
		if(left){
			return new AI(200, 200, true, Color.GREEN);
		}
		return new AI(600, 400, false, Color.RED);
	}
	
	public AI get_best_left_player() {
		return this.bots_left.get(0);
	}
	
	public AI get_best_right_player() {
		return this.bots_right.get(0);
	}
	
	public void print_results(){
		int zeros = 0;
		for(AI ai : this.bots_left) {
			if(ai.score == 0){
				zeros++;
			}
		}
		System.out.println("Zero Scores Left: " + zeros);
		zeros = 0;
		for(AI ai : this.bots_right) {
			if(ai.score == 0){
				zeros++;
			}
		}
		System.out.println("Zero Scores Right: " + zeros);
	}
}
