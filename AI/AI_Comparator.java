package AI;

import java.util.Comparator;

public class AI_Comparator implements Comparator<AI> {
	
	  @Override
	  public int compare(AI ai1, AI ai2) {
		  return ai2.compareTo(ai1);
	  }
}
	
