package AI;

import java.util.ArrayList;

import org.apache.commons.math3.linear.RealMatrix;

import Game.C;
import Game.Game;
import Game.MyUtils;
import Game.RenderObject;
import Game.Triangle;
import Game.TriangleShooterView;
import PowerUps.PowerUp;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;


public class AI_Game_Right extends Game{
	
	public double max_time = 10;
	public double total_time_passed = 0;
	public int frame_counter;
	
	public AI_Game_Right(){
		this.power_ups = new ArrayList<>();
		this.left_player = new Triangle(200, 200, true, Color.LAWNGREEN);
		AI ai = new AI(600, 400, false, Color.RED);
		ai.load_weigths(C.weight_path);
		this.right_player = ai;
	}
	
	public AI_Game_Right(AI right_player){
		this.power_ups = new ArrayList<>();
		this.right_player = right_player;
		if(!TriangleShooterView.train_bots){
			this.left_player = new Triangle(200, 200, true, Color.LAWNGREEN);
		}
	}
	
	
	public void calculate_score(){
		((AI)this.right_player).calculate_score();
	}
	
	public void update(double time_passed){
		super.update(time_passed);
		
		this.total_time_passed += time_passed;
		if(TriangleShooterView.train_bots && total_time_passed > this.max_time){
			ingame = false;
			calculate_score();
			return;
		}
		
		// the AI should only act every 5 frames
		if(this.frame_counter++ % 2 != 0) {
			return;
		}
		
		RealMatrix input = MyUtils.get_NN_input(this.right_player, this.left_projectiles, this.power_ups, this.left_player);
		((AI) this.right_player).make_move(input);
	}
	

	
	public void render(GraphicsContext gc){
		this.left_player.render(gc);
		this.right_player.render(gc);
		
		for(PowerUp p_up : power_ups){
			p_up.render(gc);
		}
		for(RenderObject projectile : left_projectiles){
			projectile.render(gc);
		}
		for(RenderObject projectile : right_projectiles){
			projectile.render(gc);
		}
	}	
}
