package AI;

import java.util.ArrayList;
import org.apache.commons.math3.linear.RealMatrix;

import Game.Game;
import Game.MyUtils;

public class AI_Game extends Game{
	
	public AI winner; // 0 if left player won, 1 otherwise
	public double max_time = 15;
	public double total_time_passed = 0;
	public int frame_counter;
	//public AI left_player;
	//public AI right_player;
	
	public AI_Game(AI left_player, AI right_player){
		this.power_ups = new ArrayList<>();
		this.left_player = left_player;
		this.right_player = right_player;
	}
	
	public void determine_winner(){
		// determine winner
		if(!ingame){
			calculate_score();
			if(((AI)right_player).score > ((AI)left_player).score){
				this.winner = (AI) right_player;
			}
			else{
				this.winner = (AI) left_player;
			}
		}
	}
	
	public void calculate_score(){
		((AI)this.left_player).calculate_score();
		((AI)this.right_player).calculate_score();
	}
	
	public void update(double time_passed){
		if(winner != null) {
			return;
		}

		super.update(time_passed);
		this.total_time_passed += time_passed;
		if(total_time_passed > this.max_time){
			ingame = false;
			determine_winner();
			return;
		}
		
		// the AI should only act every 5 frames
		if(this.frame_counter++ % 1 != 0) {
			return;
		}
		
		
		RealMatrix input = MyUtils.get_NN_input(this.left_player, this.right_projectiles, this.power_ups, this.right_player);
		((AI) this.left_player).make_move(input);
		input = MyUtils.get_NN_input(this.right_player, this.left_projectiles, this.power_ups, this.left_player);
		((AI) this.right_player).make_move(input);
		
		// increase the players scores based on their distance (The closer, the higher the score)
		double y_dist = MyUtils.get_enemy_distances(this.left_player, this.right_player)[1];
		((AI)left_player).score += Math.abs(y_dist);
		((AI)right_player).score += Math.abs(y_dist);
	}
	
	public double get_remaining_time(){
		return this.max_time - this.total_time_passed;
	}
	
}
