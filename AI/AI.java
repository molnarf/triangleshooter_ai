package AI;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Random;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.linear.*;

import Game.C;
import Game.Triangle;
import javafx.scene.paint.Color;

public class AI extends Triangle implements Comparable<AI>{
	
	public RealMatrix weights;
	public RealMatrix bias;
	public RealMatrix weights2;
	public double score;
	public double learning_rate = 0.3;
	public double mutation_prob = 0.1;
	public double threshold = 0.8;
	public boolean best_player = false;
	
	/*
	 * weights: x_pos, y_pos, x_ball, y_ball, x_p_up, y_p_up,
	 * (x_enemy, y_enemy)
	 */
	
	
	public AI(double x_pos, double y_pos, boolean left, Color color){
		super(x_pos, y_pos, left, color);
		
		// first 8 = # output, second 8 = # inputs
//		this.weights = new Array2DRowRealMatrix(new double[8][4]);
		int hidden = 20;
		
		this.weights = new Array2DRowRealMatrix(new double[hidden][20]);
		this.bias = new Array2DRowRealMatrix(new double[hidden][1]);
		this.weights2 = new Array2DRowRealMatrix(new double[4][hidden]);
	}
	
	
	public AI(double x_pos, double y_pos, boolean left, Color color, RealMatrix weights, RealMatrix bias){
		this(x_pos, y_pos, left, color);
		this.weights = weights;
	}
	
	public AI(double x_pos, double y_pos, boolean left, Color color, RealMatrix weights, RealMatrix bias,
			RealMatrix weights2){
		this(x_pos, y_pos, left, color, weights, bias);
		this.bias = bias;
		this.weights2 = weights2;
	}
	
	public AI(AI ancestor){
//		this(ancestor.x_pos, ancestor.y_pos, ancestor.left, ancestor.color, ancestor.weights.copy(), ancestor.bias.copy());
		this(ancestor.x_pos, ancestor.y_pos, ancestor.left, ancestor.color, ancestor.weights.copy(), ancestor.bias.copy(),
				ancestor.weights2.copy());
		reset_position();
	}
	
	public AI crossover(AI partner) {
		RealMatrix w1 = crossover_matrix(this.weights, partner.weights);
		RealMatrix b1 = crossover_matrix(this.bias, partner.bias);
		RealMatrix w2 = crossover_matrix(this.weights2, partner.weights2);
		AI child = new AI(0, 0, left, color, w1, b1, w2);
		child.reset_position();
		return child;
	}
	
	
	// cross m1 and m2 over at a random position
	public RealMatrix crossover_matrix(RealMatrix m1, RealMatrix m2) {
		Random rd = new Random();
		int row = rd.nextInt(m1.getRowDimension());
		int col = rd.nextInt(m1.getColumnDimension());
		RealMatrix new_matrix = new Array2DRowRealMatrix(m1.getRowDimension(), m1.getColumnDimension());
		
		for(int i = 0; i<m1.getRowDimension(); i++) {
			for(int j = 0; j<m1.getColumnDimension(); j++) {
				if(i < row || row == i && j<col) {
					new_matrix.setEntry(i, j, m1.getEntry(i, j));
				}
				else {
					new_matrix.setEntry(i, j, m2.getEntry(i, j));
				}
			}
		}
		
		return new_matrix;
	}
	
	public void init(double x_pos, double y_pos, boolean left, Color color) {
		this.x_pos = x_pos;
		this.y_pos = y_pos;
		super.init(left, color);
		this.score = 0;
		power_ups_collected = 0;
		wall_touches = 0;
		cooldown_time = 1.2 / C.game_speed;
		multishot_time = 0;
		invincibility_time = 0;
		ball_diameter = 25;
	}
	
	public void mutate2() {
		mutateMatrix(weights);
		mutateMatrix(bias);
		mutateMatrix(weights2);
	}
	
	public void init_weights() {
		initialize_randomly(weights);
		initialize_randomly(bias);
		initialize_randomly(weights2);
	}
	
	public void mutate(){
		mutateMatrix(weights);
	}
	
	public void calculate_score() {
		score += power_ups_collected * 100;
		score -= wall_touches * 2;
		score += lives * 10;
		score *= lives;
		if(lives == 0) {
			score = 0;
		}
		score = Math.max(score, 0);
	}

	// fills the matrix with random values between -2 and 2
	public void initialize_randomly(RealMatrix matrix) {
		Random r = new Random();
		for(int i = 0; i<matrix.getRowDimension(); i++) {
			for(int j = 0; j<matrix.getColumnDimension(); j++) {
				double randomValue = (r.nextDouble() - 0.5) *4;
				matrix.setEntry(i, j, randomValue);
			}
		}
	}
	
	public void mutateMatrix(RealMatrix matrix) {
		Random r = new Random();
		NormalDistribution norm = new NormalDistribution(0, 1);

		for(int i = 0; i<matrix.getRowDimension(); i++){
			for(int j = 0; j<matrix.getColumnDimension(); j++){
				double randomValue = r.nextDouble();
				if(randomValue < mutation_prob) {
					randomValue = r.nextDouble();
					if(randomValue < 0.9){	// 90% chance to adjust
						matrix.setEntry(i, j, matrix.getEntry(i, j) + learning_rate * norm.sample());
					}
					else{					// 10% chance to draw new weight
						matrix.setEntry(i, j, (r.nextDouble() - 0.5) *4);
					}
					if(matrix.getEntry(i, j) > 2) {
						matrix.setEntry(i, j, 2);
					}
					else if(matrix.getEntry(i, j) < -2) {
						matrix.setEntry(i, j, -2);
					}
				}
			}
		}
	}

	
	public void ReLu(RealMatrix matrix) {
		for(int i = 0; i<matrix.getRowDimension(); i++) {
			matrix.setEntry(i, 0, Math.max(0, matrix.getEntry(i, 0)));
		}
	}
	
	public void sigmoid(RealMatrix matrix) {
		for(int i = 0; i<matrix.getRowDimension(); i++) {
			matrix.setEntry(i, 0, 1.0/(1 + Math.pow(Math.E, -matrix.getEntry(i, 0))));
		}
	}
	
	
	public void make_move(RealMatrix input){
		// reset acceleration before making the next move
		this.x_dir = 0;
		this.y_dir = 0;
		
		RealMatrix output = this.weights.multiply(input);
		
		// for an additional layer
		output = output.add(this.bias);
		sigmoid(output);
		output = this.weights2.multiply(output);
		sigmoid(output);
		
		if(output.getEntry(0, 0) > threshold) {
			go_left();
		}
		else {
			if(output.getEntry(1, 0) > threshold) {
				go_right();
			}
		}
		if(output.getEntry(2, 0) > threshold) {
			go_up();
		}
		else {
			if(output.getEntry(3, 0) > threshold) {
				go_down();
			}
		}
		
		// punish just standing around
		double max = 0;
		for(int i = 0; i<output.getRowDimension(); i++){
			if(output.getEntry(i, 0) > max){
				max = output.getEntry(i, 0);
			}
		}
//		if(max < threshold) {
//			this.wall_touches++;
//		}
	}
	
	
	
	public void save_weights(String path) {
		path = path + "2";
		path = get_correct_weight_path(path);
		try {
			File file = new File(path);
			file.getParentFile().mkdirs();
			PrintWriter pw = new PrintWriter(new FileWriter(path));
			pw.write(matrix_to_string(weights) + "\n");
			pw.write(matrix_to_string(bias) + "\n");
			pw.write(matrix_to_string(weights2));
			pw.flush();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void load_weigths(String path) {
		path = path + "2";
		path = get_correct_weight_path(path);
		int counter = 0;
		try {
			URL url = ClassLoader.getSystemResource(path);
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
			String line;
			while((line = br.readLine()) != null) {
				line = line.replace('{', ' ');
				line = line.replace(',', '.');
				String[] rows = line.split("}");
				int n_rows = rows.length;
				int n_cols = rows[0].split("; ").length;
				double[][] array = new double[n_rows][n_cols];
				for(int i = 0; i<n_rows; i++) {
					String[] values = rows[i].split("; ");
					for(int j = 0; j<n_cols; j++) {
						double weight = Double.parseDouble(values[j]);
						array[i][j] = weight;
					}
				}
				
				if(counter == 0) {
					this.weights = new Array2DRowRealMatrix(array);
				}
				else if(counter == 1) {
					this.bias = new Array2DRowRealMatrix(array);
				}
				else if(counter == 2) {
					this.weights2 = new Array2DRowRealMatrix(array);
				}
				counter++;
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String get_correct_weight_path(String path) {
		if(this.left) {
			path = path + "_left.txt";
		}
		else {
			path = path + "_right.txt";
		}
		return path;
	}

	public String matrix_to_string(RealMatrix matrix) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i<matrix.getRowDimension(); i++) {
			RealVector row = matrix.getRowVector(i);
			sb.append(row.toString());
		}
		
		return sb.toString();
	}
	
	public void reset_position() {
		if(left){
			this.x_pos = 200;
			this.y_pos = 200;
		}
		else{
			this.x_pos = 600;
			this.y_pos = 400;
		}
	}

	@Override
	public int compareTo(AI other) {
		if(this.score > other.score) {
			return 1;
		}
		else if(this.score < other.score) {
			return -1;
		}
		return 0;
	}
	
}
