package PowerUps;

import Game.C;
import Game.Triangle;

public class GiantShot extends PowerUp{
	
	public GiantShot(double x_pos, double y_pos){
		super(x_pos, y_pos);
		this.image = C.giant_shot_image; 
	}

	@Override
	public void activate(Triangle player) {
		player.ball_diameter += 4;
		collect_sound();
	}

}
