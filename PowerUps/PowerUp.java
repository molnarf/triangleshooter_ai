package PowerUps;

import Game.C;
import Game.RenderObject;
import Game.Triangle;
import Game.TriangleShooterView;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

public abstract class PowerUp extends RenderObject{
	public Image image;
	public static double width = 40;
	public static double height = 40;
	public MediaPlayer mediaPlayer = new MediaPlayer(C.power_up_sound);
	
	public PowerUp(double x_pos, double y_pos) {
		super(x_pos, y_pos, width, height);
	}
	
	public abstract void activate(Triangle player);
	
	public Shape getBoundary() {
		Shape shape = new Rectangle(x_pos, y_pos, width, height);
		return shape;
	}

	@Override
	public void render(GraphicsContext gc){
		gc.setStroke(Color.BLUE);
		gc.strokeRect(x_pos, y_pos, width, height);
		gc.drawImage(image, x_pos, y_pos);
	}
	
	
	public void collect_sound(){
		if(!TriangleShooterView.everything_muted && !TriangleShooterView.train_bots){
			this.mediaPlayer.play();
		}
	}
	
	
}
