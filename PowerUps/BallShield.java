package PowerUps;


import Game.C;
import Game.Triangle;
import Game.TriangleShooterView;

public class BallShield extends PowerUp{
	
	public BallShield(double x_pos, double y_pos){
		super(x_pos, y_pos);
		this.image = C.ball_shield_image;
		if(!TriangleShooterView.train_bots) {
			this.image = C.ball_shield_image;
		}
	}

	@Override
	public void activate(Triangle player) {
		player.add_shield_balls();
		collect_sound();
	}

}
