package PowerUps;

import Game.C;
import Game.Triangle;

public class MultiShot extends PowerUp{

	public double duration = 3 / C.game_speed;
	
	public MultiShot(double x_pos, double y_pos){
		super(x_pos, y_pos);
		this.image = C.multi_shot_image;
	}
	
	
	public void activate(Triangle player){
		player.multishot_time = this.duration;
		collect_sound();
	}
	
	
}
