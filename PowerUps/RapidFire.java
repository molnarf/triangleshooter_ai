package PowerUps;

import Game.C;
import Game.Triangle;


public class RapidFire extends PowerUp{

	public RapidFire(double x_pos, double y_pos){
		super(x_pos, y_pos);
		this.image = C.rapid_fire_image;
	}
	
	
	public void activate(Triangle player){
		player.cooldown_time /= 1.25;
		collect_sound();
	}
	
}
