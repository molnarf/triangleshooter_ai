package Game;
import javafx.animation.AnimationTimer;

public class MyAnimationTimer extends AnimationTimer{

	
	private TriangleShooterView view;
	private long time_last_frame;
	private boolean running;
	
	public MyAnimationTimer(TriangleShooterView view){
		this.view = view;
	}
		
	@Override
	public void handle(long currentNanoTime){
		double time_passed_secs = (currentNanoTime - time_last_frame) / 1000000000.0;
		if(time_passed_secs > 2){
			time_passed_secs = 0;	// for the first frame
		}
		time_last_frame = currentNanoTime;

		if(TriangleShooterView.train_bots){
			view.population.update(time_passed_secs);
			view.paintComponents_bot();
			if(view.population.generation_finished){
				this.stop();
				System.out.println("\nCreating offspring ;)  " + view.population.generation);
				view.population.createOffspring();
				System.gc();
				this.start();
			}
			return;
		}
		
		if(!view.game.ingame){
			this.stop();
			view.winnerText();
			view.resetButton();
		}
		else{
			view.game.update(time_passed_secs);
			view.paintComponents();
		}
		
	}
	
	public void start() {
		super.start();
		this.running = true;
	}
	
	public void stop() {
		super.stop();
		this.running = false;
	}
	
	public boolean isRunning() {
		return this.running;
	}
	
	
}
