package Game;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;

public class Ball extends RenderObject{
	
	//public static final double diameter = 25;
	//public static final double radius = diameter/2;
	public double diameter;
	public double radius;
	public Color color;
	
	public Ball(double x_pos, double y_pos, double x_dir, double y_dir, double diameter, Color color){
		super(x_pos, y_pos);
		this.y_pos = y_pos - diameter/2;
		this.x_dir = x_dir * C.game_speed;
		this.y_dir = y_dir * C.game_speed;
		this.diameter = diameter;
		this.width = diameter;
		this.height = diameter;
		this.radius = diameter/2;
		this.color = color;
	}

	public Ball(double x_pos, double y_pos, double x_dir, double y_dir){
		this(x_pos, y_pos, x_dir, y_dir, 25, Color.BLACK);
	}
	
	public Shape getBoundary(){
		Shape shape = new Circle(x_pos+radius, y_pos+radius, radius);
		return shape;
	}
	
	public boolean within_y_boundary(double pos) {
		if(pos >= this.center_y-radius && pos <= this.center_y+radius) {
			return true;
		}
		return false;
	}
	
	public void transpose(double x_dist, double y_dist){
		this.x_pos += x_dist;
		this.y_pos += y_dist;
		this.center_x += x_dist;
		this.center_y += y_dist;
	}
	
	public void render(GraphicsContext gc){
		gc.setFill(color);
		gc.setLineDashes(0);
		gc.setStroke(Color.WHITE);
		gc.fillOval(x_pos, y_pos, diameter, diameter);
		gc.strokeOval(x_pos, y_pos, diameter, diameter);
	}
	
	public void render_speed_lines(GraphicsContext gc){
		int num_lines = 5;
		int max_idx = num_lines/2;
		double max_line_width = 1.5*diameter;
		double min_line_with = 0.8*diameter;
		double offset = 0.08 * diameter;
		for(int i = 0; i<num_lines; i++){
			double line_width = max_line_width -  min_line_with * (Math.abs(max_idx - i)/(double)max_idx);
			if(x_dir > 0){
				gc.fillOval(x_pos-line_width-offset, y_pos+diameter*((double)i/num_lines), line_width, diameter/8);	// ball flying to the right
			}
			else{
				gc.fillOval(x_pos+diameter+offset, y_pos+diameter*((double)i/num_lines), line_width, diameter/8);	// ball flying to the left
			}
		}
	}
	
	public String toString(){
		return "x: " + this.center_x + "    y: " + this.center_y;
	}
	
}
