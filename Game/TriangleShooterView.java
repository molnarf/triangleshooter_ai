package Game;

import java.text.DecimalFormat;

import AI.AI_Game_Right;
import AI.Population;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class TriangleShooterView extends Application{
	
	public Game game;
	private GraphicsContext gc;
	public Group root;
	public static final int width = 800;
	public static final int height = 500;
	public MyAnimationTimer timer;
	public MediaPlayer mediaPlayer;
	public Image background;
	public Menu menu;
	public static boolean everything_muted = false;
	
	public static boolean train_bots = false;
	public Population population;
//	public PopulationRight population;
	
	
	public void createWindow(String[] args){
		launch(args);
	}
	
	public void paintComponents(){
		gc.drawImage(background, 0, 0);
		
		gc.setStroke(Color.ORANGE);
		gc.setLineWidth(3);
		gc.setLineDashes(10);
		gc.strokeLine(width/2, 0, width/2, height);
		
		this.game.render(gc);
	}
	
	public void paintComponents_bot(){
		gc.drawImage(background, 0, 0);

		gc.setStroke(Color.ORANGE);
		gc.setLineWidth(3);
		gc.setLineDashes(10);
		gc.strokeLine(width/2, 0, width/2, height);
		gc.setFill(Color.SPRINGGREEN);
		gc.fillText("Gen: " + population.generation, width-100, 40);
		gc.fillText("Player #" + population.view_player_nr, 20, 40);
		DecimalFormat df = new DecimalFormat("#.##");
		gc.fillText("Time left: " + df.format(this.population.games.get(0).get_remaining_time()), 150, 40);
		this.population.render(gc);
	}
	
	
	public void resetButton(){
		VBox vBox = new VBox();
		vBox.setMinSize(width, 50);
		vBox.setLayoutY(height/2 - 50);
		vBox.setAlignment(Pos.CENTER);
		
		Button button = new Button("Reset");
		Text text = winnerText();
		vBox.getChildren().add(text);
		vBox.getChildren().add(button);
		
		button.setOnAction(event ->{
			if(game instanceof AI_Game_Right){
				this.game = new AI_Game_Right();
			}
			else{
				this.game = new Game(true);
			}
        	this.root.getChildren().remove(vBox);
        	this.timer.start();
        });
		this.root.getChildren().add(vBox);
	}
	
	public Text winnerText(){
		Color winnerColor = game.winner.color;
		String text = "Red wins!";
		if(game.winner.left){
			text = "Green wins!";
		}
		Text t = new Text(text);
		t.setFill(winnerColor);
		t.setFont(new Font(50));
		return t;
	}
	
	public void openMenu(){
		this.timer.stop();
		menu.open();
	}
	
	public void closeMenu(){
		this.timer.start();
		menu.close();
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception{
		if(train_bots) {
			this.population = new Population();
//			this.population = new PopulationRight();
		}
		else {
			this.game = new Game(true);
		}
		this.root = new Group();
		Scene scene = new Scene(root);
		scene.setOnKeyPressed(new TAdapter());
		scene.setOnKeyReleased(new TAdapter());
        Canvas canvas = new Canvas(width, height);
		root.getChildren().add(canvas);
		this.gc = canvas.getGraphicsContext2D();
		gc.setFont(new Font(24));
		
		// set background image
		this.background = C.background_image;
		gc.drawImage(background, 0, 0);
		this.timer = new MyAnimationTimer(this);
		this.menu = new Menu(this);

		primaryStage.setScene(scene);
		primaryStage.setTitle("Triangle Shooter");
		primaryStage.show();
		
		// play song
		this.mediaPlayer = new MediaPlayer(C.soundtrack);
		mediaPlayer.play();
	}

	
	 private class TAdapter implements javafx.event.EventHandler<KeyEvent>{

	 	@Override
		public void handle(KeyEvent event) {
			// TODO Auto-generated method stub
			if(event.getEventType() == KeyEvent.KEY_PRESSED){
				if(event.getCode() == KeyCode.P) {
					if(timer.isRunning()) {
						timer.stop();
					}
					else {
						timer.start();
					}
					return;
				}
				else if(event.getCode() == KeyCode.L) {
					C.game_speed += 0.1;
				}
				else if(event.getCode() == KeyCode.K) {
					C.game_speed -= 0.1;
				}
				else if(event.getCode() == KeyCode.ESCAPE){
					if(!menu.is_open){
						openMenu();
						menu.is_open = true;
					}
					else{
						closeMenu();
						menu.is_open = false;
					}
				}
				if(train_bots) {
					if(event.getCode() == KeyCode.N) {
//						population.get_best_left_player().save_weights(C.weight_path);
						population.get_best_right_player().save_weights(C.weight_path);
					}
					else if(event.getCode() == KeyCode.M) {
//						population.get_best_left_player().load_weigths(C.weight_path);
						population.get_best_right_player().load_weigths(C.weight_path);
					}
					if(event.getCode() == KeyCode.LEFT){
						population.view_player_nr = Math.max(0, population.view_player_nr-1);
					}
					if(event.getCode() == KeyCode.RIGHT){
						population.view_player_nr = Math.min(population.population_size, population.view_player_nr+1);
					}
				}
				else {
					game.keyPressed(event);
				}
			}
			if(event.getEventType() == KeyEvent.KEY_RELEASED && !train_bots){
				game.keyReleased(event);
			}
		}
		
	}
}
