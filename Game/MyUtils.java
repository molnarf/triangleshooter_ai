package Game;

import java.util.ArrayList;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

import PowerUps.PowerUp;

public class MyUtils {
	
	public static String arrayToString(double[] array){
		String output = "";
		
		for(int i = 0; i<array.length; i++){
			output += array[i] + "  ";
		}
		
		return output;
	}
	
	public static String arrayToString(double[][] array){
		String output = "";
		for(int i = 0; i<array.length; i++){
			for(int j = 0; j<array[0].length; j++){
				output += array[i][j] + " ";
			}
			output += "\n";
		}
		return output;
	}
	
	
	public static double[] append_arrays(double[] a1, double[] a2){
		double[] joined = new double[a1.length + a2.length];
		for(int i = 0; i<a1.length; i++){
			joined[i] = a1[i];
		}
		for(int i = 0; i<a2.length; i++){
				joined[i+a1.length] = a2[i];
		}
		return joined;
	}
	
	public static double distance(Triangle player, RenderObject object){
		return(Math.sqrt(Math.pow(player.x_pos - object.center_x, 2) + Math.pow(player.y_pos - object.center_y, 2)));
	}
	
	
	// Neural Network Functions
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	
	
	
	public static RealMatrix get_NN_input(Triangle player, ArrayList<Ball> projectiles, ArrayList<PowerUp> power_ups, Triangle enemy){
		// The x and y positions are with respect to the middle of the respective left and right field
		double[] ball_distances = get_ball_direction_distances(player, projectiles);
		double[] wall_distances = get_wall_distances(player);
		double[] p_up_directions = get_p_up_directions(player, power_ups);
		double[] enemy_pos = get_enemy_distances(player, enemy);
		
		double[] tmp = append_arrays(MyUtils.append_arrays(ball_distances, wall_distances), p_up_directions);
		tmp = append_arrays(tmp, enemy_pos);
		RealMatrix input = new Array2DRowRealMatrix(tmp);
		return input;
	}
	
	public static double[] get_enemy_distances(Triangle player, Triangle enemy){
		double[] distances = new double[2];
		distances[0] = 2 / (player.x_pos - enemy.x_pos + 2);
		distances[1] = 2 / (player.y_pos - enemy.y_pos + 2);
		return distances;
	}
	
	public static double[] get_wall_distances(Triangle player){
		double[] distances = new double[4];	
		distances[0] = player.y_pos;														// dist to top
		distances[1] = TriangleShooterView.height - player.y_pos;							// dist to bot
		if(player.left){
			distances[2] = player.get_center_x();													// dist to left
			distances[3] = TriangleShooterView.width/2 - player.get_center_x();					// dist to right
		}
		else{
			distances[2] = player.get_center_x() - TriangleShooterView.width/2;					// dist to left
			distances[3] = TriangleShooterView.width - player.get_center_x();						// dist to right		
		}
		for(int i = 0; i<distances.length; i++){
			distances[i] = 100 / distances[i];
		}
		return distances;
	}
	
	public static double[] get_p_up_directions(Triangle player, ArrayList<PowerUp> power_ups){
		double closest_p_up = 100000;
		// the default power up value when none is present is in the middle of the field (relative to the player)
		double p_up_y = 0;		
		double p_up_x = 0;
		
		for(PowerUp p_up : power_ups){
			if(p_up_is_in_right_half(player, p_up)){
				if(distance(player, p_up) < closest_p_up){
					closest_p_up = distance(player, p_up);
					p_up_x = get_x_dir_to_p_up(player, p_up);
					p_up_y = get_y_dir_to_p_up(player, p_up);
				}
			}
		}
		return new double[]{p_up_x, p_up_y};
	}
	
	public static boolean p_up_is_in_right_half(Triangle player, PowerUp p_up){
		if((p_up.center_x >= TriangleShooterView.width/2 && !player.left) ||	// if the player and the powerup is left
			(p_up.center_x <= TriangleShooterView.width/2 && player.left)){		// if the player and the powerup is right
			return true;
		}
		return false;
	}
	
	public static double[] get_ball_direction_distances(Triangle player, ArrayList<Ball> balls){
		double[] slopes = new double[]{-10, -2, -1, -0.4, -0.1, 0, 0.1, 0.4, 1, 2, 10};
		double[] distances = new double[slopes.length + 1];
		for(int i = 0; i<distances.length-1; i++){
			distances[i] = get_dist_to_closest_ball(player, balls, slopes[i]);
		}
		// the last entry is the size of the ball
		double ball_size = 1;
		if(balls.size() > 0){
			ball_size = balls.get(0).diameter/25;
		}
		distances[distances.length-1] = ball_size;
		return distances;
	}
	
	
	public static double get_dist_to_closest_ball(Triangle player, ArrayList<Ball> balls, double slope) {
		double closest_dist = 100000000;																
			for(Ball ball : balls){//																						  O
				if(player.ball_is_in_front_of_player(ball)){											//  			    /
					// transpose the ball temporarily to simulate looking in a diagonal direction		slope of 1:  	  /
					double x_transpose = player.get_center_x() - ball.center_x;							//				O
					double y_transpose = -(x_transpose * slope);
					if(player.would_hit_transposed_ball(ball, x_transpose, y_transpose)){
						if(MyUtils.distance(player, ball) < closest_dist) {
							closest_dist = MyUtils.distance(player, ball);
						}
					}
				}
			}
		return 200/closest_dist;
	}
	
	
	/**
	 * return 0 when the players x-position is already within the powerup
	 * else return the direction to the powerup (either 1 or -1)
	 * @param player
	 * @param p_up
	 * @return
	 */
	public static double get_x_dir_to_p_up(Triangle player, RenderObject p_up) {
		double x_dir = 0;
		if(p_up.center_x - PowerUp.width/2 < player.x_pos && 
		   p_up.center_x + PowerUp.width/2 > player.x_pos) {
				x_dir = 0;
		}
		else {
			x_dir = Math.signum(p_up.center_x - player.x_pos);
		}
		return x_dir;
	}
	
	public static double get_y_dir_to_p_up(Triangle player, RenderObject obj) {
		double y_dir = 0;
		double height = PowerUp.height;
		if(obj instanceof Ball) {
			height = ((Ball) obj).diameter;
		}
		if(obj.center_y - height/2 < player.y_pos && 
		   obj.center_y + height/2 > player.y_pos) {
				y_dir = 0;
		}
		else {
			y_dir = Math.signum(obj.center_y - player.y_pos);
		}
		return y_dir;
	}

	
}
