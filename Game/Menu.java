package Game;

import AI.AI;
import AI.AI_Game_Right;
import AI.Population;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class Menu {

	public TriangleShooterView view;
	public boolean is_open;
	public VBox game_options;
	public VBox mute_box;
	
	public Menu(TriangleShooterView view){
		this.view = view;
		this.game_options = new VBox();
		this.mute_box = new VBox();
		addButtons();
	}
	
	public void open(){
		view.root.getChildren().addAll(game_options, mute_box);
	}
	
	public void close(){
		view.root.getChildren().removeAll(game_options, mute_box);
	}
	
	public void addButtons(){
		game_options.setPrefSize(300, 300);
		game_options.setMinSize(300, 300);
		game_options.setMaxSize(300, 300);
		game_options.setLayoutY(TriangleShooterView.height/2- game_options.getPrefHeight()/2);
		game_options.setLayoutX(TriangleShooterView.width/2 - game_options.getPrefWidth()/2);
		game_options.setAlignment(Pos.CENTER);
		game_options.setSpacing(10);
		
		mute_box.setMinSize(150, 100);
		mute_box.setLayoutX(TriangleShooterView.width-200);
		mute_box.setAlignment(Pos.CENTER);
		mute_box.setSpacing(10);
		
		Button pvp_button = new Button("Player vs Player");
		pvp_button.setMinWidth(game_options.getPrefWidth());
		pvp_button.setOnAction(event ->{
			TriangleShooterView.train_bots = false;
			view.game = new Game(true);
			view.root.getChildren().removeAll(game_options, mute_box);
			view.timer.start();
		});
		
		Button p_vs_ai_button = new Button("Player vs AI");
		p_vs_ai_button.setMinWidth(game_options.getPrefWidth());
		p_vs_ai_button.setOnAction(event ->{
			TriangleShooterView.train_bots = false;
			AI ai = new AI(600, 400, false, Color.RED);
			ai.load_weigths(C.weight_path);
			view.game = new AI_Game_Right(ai);
			view.root.getChildren().removeAll(game_options, mute_box);
			view.timer.start();
		});
		
		Button ai_vs_ai_button = new Button("AI vs AI");
		ai_vs_ai_button.setMinWidth(game_options.getPrefWidth());
		ai_vs_ai_button.setOnAction(event ->{
			TriangleShooterView.train_bots = true;
			view.population = new Population();
			view.root.getChildren().removeAll(game_options, mute_box);
			view.timer.start();
		});
		game_options.getChildren().addAll(pvp_button, p_vs_ai_button, ai_vs_ai_button);
		
		setPurpleButtonStyle(pvp_button, p_vs_ai_button, ai_vs_ai_button);
		
		Button mute_music_button = new Button("Mute Music");
		mute_music_button.setMinSize(mute_box.getMinWidth(), 40);
		mute_box.getChildren().add(mute_music_button);
		mute_music_button.setOnAction(event ->{
			view.mediaPlayer.setMute(!view.mediaPlayer.isMute());
			if(view.mediaPlayer.isMute()){
				setRedButtonStyle(mute_music_button);
			}
			else{
				setGreenButtonStyle(mute_music_button);
			}
		});
		
		Button mute_all_button = new Button("Mute Everything");
		setGreenButtonStyle(mute_music_button, mute_all_button);
		mute_all_button.setMinSize(mute_box.getMinWidth(), 40);
		mute_box.getChildren().add(mute_all_button);
		mute_all_button.setOnAction(event ->{
			TriangleShooterView.everything_muted = !TriangleShooterView.everything_muted;
			if(TriangleShooterView.everything_muted){
				view.mediaPlayer.setMute(true);
				setRedButtonStyle(mute_music_button, mute_all_button);
			}
			else{
				view.mediaPlayer.setMute(false);
				setGreenButtonStyle(mute_music_button, mute_all_button);
			}
		});
		
		view.root.getChildren().add(game_options);
		view.root.getChildren().add(mute_box);
		
	}
	
	public static void setPurpleButtonStyle(Button... buttons){
		for(Button button: buttons){
			button.setStyle("-fx-background-color: purple; -fx-text-fill: white; -fx-font-size: 25px; -fx-border-color: white; -fx-border-width: 3px;");
		}
	}
	
	public static void setRedButtonStyle(Button...buttons){
		for(Button button: buttons){
		button.setStyle("-fx-background-color: red; -fx-border-color: white; -fx-border-width: 3px; -fx-text-fill: white; -fx-font-size: 15px;");
		}
	}
	
	public static void setGreenButtonStyle(Button...buttons){
		for(Button button: buttons){
			button.setStyle("-fx-background-color: green; -fx-border-color: white; -fx-border-width: 3px; -fx-text-fill: white; -fx-font-size: 15px;");
		}
	}
	
}
