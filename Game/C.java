package Game;

import java.net.URL;

import javafx.scene.image.Image;
import javafx.scene.media.Media;

public class C {

	// constant values
	public static double game_speed = 1.0;
	public static double p_up_width = 40;
	public static double p_up_height = 40;
	public static double dist_scaling = Math.sqrt(Math.pow(TriangleShooterView.width, 2) + Math.pow(TriangleShooterView.height, 2));
	public static String weight_path = "Media/weights/best";
	
	// load images
	public static Image background_image = load_image("Media/Background.JPG", TriangleShooterView.width, TriangleShooterView.height);
	public static Image giant_shot_image = load_image("Media/GiantShot.JPG", p_up_width, p_up_height);
	public static Image ball_shield_image = load_image("Media/BallShield.JPG", p_up_width, p_up_height);
	public static Image multi_shot_image = load_image("Media/MultiShot.JPG", p_up_width, p_up_height);
	public static Image rapid_fire_image = load_image("Media/rapidfire2.JPG", p_up_width, p_up_height);
	public static Image massalski = load_image("Media/Massalski.PNG", 30, 40);
	public static Image explosion_image = load_image("Media/explosion 3.png", 800, 800);
	
	// load sounds
	public static Media laser_sound = load_sound("Media/laser_shot.mp3");
	public static Media boom_sound = load_sound("Media/BOOM.mp3");
	public static Media soundtrack = load_sound("Media/disco_soundtrack.mp3");
	public static Media power_up_sound = load_sound("Media/power_up.mp3");

	
	// loading functions
	public static Image load_image(String path, double width, double height) {
		URL url = ClassLoader.getSystemResource(path);
		return new Image(url.toString(), width, height, false, false);
	}
	
	public static Media load_sound(String path) {
		URL url = ClassLoader.getSystemResource(path);
        return new Media(url.toString());
	}
}
