package Game;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.shape.Shape;

public abstract class RenderObject {

	public double x_pos;
	public double y_pos;
	public double x_dir;
	public double y_dir;
	public double width;
	public double height;
	public double center_x;
	public double center_y;

	public RenderObject(double x_pos, double y_pos) {
		init(x_pos, y_pos);
	}
	
	public RenderObject(double x_pos, double y_pos, double width, double height) {
		this.width = width;
		this.height = height;
		init(x_pos, y_pos);
	}
	
	public void init(double x_pos, double y_pos) {
		this.x_pos = x_pos;
		this.y_pos = y_pos;
		this.center_x = this.x_pos + 0.5 * width;
		this.center_y = this.y_pos + 0.5 * height;
	}
	
	public void move(){
		this.x_pos += x_dir;
		this.y_pos += y_dir;
		this.center_x = this.x_pos + 0.5 * width;
		this.center_y = this.y_pos + 0.5 * height;
	}
	
	public abstract Shape getBoundary();
	
	public abstract void render(GraphicsContext gc);
	
}
