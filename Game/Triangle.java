package Game;

import java.util.ArrayList;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;
import javafx.scene.shape.Polygon;

public class Triangle extends RenderObject{
	
	// left or right player
	public boolean left;
	public double cooldown_time = 1.2 / C.game_speed;
	public double current_cooldown;
	public double multishot_time = 0;
	public double invincibility_time = 0;
	public double invincibility_duration = 1 / C.game_speed;
	public double ball_diameter = 25;
	public MediaPlayer shoot_sound;
	public MediaPlayer explosion_sound;
	public Color color;
	public int lives;
	public Image live_image;
	public Image explosion_image;
	public int current_img_idx;
	public boolean exploding = false;
	public double speed = 7.5 * C.game_speed;
	public ArrayList<RotatingBall> shield_balls;
	
	// for score calculation
	public int power_ups_collected = 0;
	public int wall_touches = 0;
	
	public Triangle(double x_pos, double y_pos, boolean left, Color color){
		super(x_pos, y_pos);
		init(left, color);
		init_media();
	}
	
	public void init(boolean left, Color color) {
		this.width = 40;
		this.height = 15;
		this.left = left;
		this.lives = 3;
		this.shield_balls = new ArrayList<>();
		this.color = color;
		
		if(!left){
			x_pos -= width;
		}
	}
	
	public void init_media() {
		this.live_image = C.massalski;
		this.explosion_image = C.explosion_image;
		if(!TriangleShooterView.train_bots){
			this.shoot_sound = new MediaPlayer(C.laser_sound);
			this.explosion_sound = new MediaPlayer(C.boom_sound);
		}
	}
	
	
	public void move(){
		double left_boundary = 0;
		double right_boundary = TriangleShooterView.width/2;
		if(!left){
			left_boundary = TriangleShooterView.width/2 - width;
			right_boundary = TriangleShooterView.width - width;
		}
		// if the x position is within the boundaries
		if(!(x_pos-width < left_boundary && x_dir < 0) && !(x_pos > right_boundary && x_dir > 0)){
			x_pos += x_dir;
		}
		else {
			this.wall_touches++;
		}
	
		if(!(y_pos-height < 0 && y_dir < 0) && !(y_pos+height > TriangleShooterView.height && y_dir > 0)){
			y_pos += y_dir;
		}
		else {
			this.wall_touches++;
		}
		for(RotatingBall shield: this.shield_balls) {
			shield.move(this.x_pos, this.y_pos);
		}
	}
	
	public void shoot(ArrayList<Ball> balls ){
		if(this.current_cooldown > 0){
			return;
		}
		
		double direction = 10;
		double ball_x_pos = x_pos;
		if(!left){
			direction = -direction;
			ball_x_pos -= ball_diameter;
		}
		balls.add(new Ball(ball_x_pos, y_pos, direction, 0, ball_diameter, color));
		
		if(multishot_time > 0){
			balls.add(new Ball(ball_x_pos, y_pos, direction, -3, ball_diameter, color));
			balls.add(new Ball(ball_x_pos, y_pos, direction, 3, ball_diameter, color));
		}
		this.current_cooldown = cooldown_time;
		shoot_sound();
		
		return;
	}
	
	public void add_shield_balls() {
		this.shield_balls.add(new RotatingBall(this.x_pos, this.y_pos, 100, 30, Color.YELLOW));
	}
	
	public void shoot_sound(){
		if(this.shoot_sound != null && !TriangleShooterView.everything_muted) {
			this.shoot_sound.stop();
			this.shoot_sound.play();
		}
	}
	
	public void explosion_sound(){
		if(this.explosion_sound != null && !TriangleShooterView.everything_muted) {
			this.explosion_sound.stop();
			this.explosion_sound.play();
		}
	}
	
	public Shape getBoundary(){
		if(left){
			return new Polygon(new double[]{x_pos, y_pos, x_pos-width, y_pos-height, x_pos-width, y_pos+height});
		}
		else{
			return new Polygon(new double[]{x_pos, y_pos, x_pos+width, y_pos-height, x_pos+width, y_pos+height});
		}
	}
	
	public boolean shield_intersects(Shape s, Color color) {
		if(color == this.color) {
			return false;
		}
		for(int i = 0; i<this.shield_balls.size(); i++) {
			Shape intersection = Shape.intersect(s, shield_balls.get(i).getBoundary());
			if (((Path) intersection).getElements().size() > 0) {		// checks if the intersection is empty between both shapes
	            this.shield_balls.remove(i);
				return true;
	        }
		}
		return false;
	}
	
	public boolean intersects(Shape s){
    	Shape intersection = Shape.intersect(s, this.getBoundary());
    	if (((Path) intersection).getElements().size() > 0) {		// checks if the intersection is empty between both shapes
            return true;
        }
    	return false;
    }
	

	public boolean ball_is_in_front_of_player(Ball ball){
		if(left){
			return ball.center_x + ball.radius > this.x_pos - this.width;
		}
		return ball.center_x - ball.radius < this.x_pos + this.width;
	}
	
	public boolean would_hit_ball(Ball ball) {
		// return true if the ball is bigger than the triangle and would hit it
		if(ball.center_y - ball.radius < this.y_pos - this.height && ball.center_y + ball.radius > this.y_pos + this.height){
			return true;
		}
		return y_within_player_height(ball.center_y - ball.radius) || y_within_player_height(ball.center_y + ball.radius);
	}
	
	public boolean would_hit_transposed_ball(Ball ball, double x_transpose, double y_transpose){
		ball.transpose(x_transpose, y_transpose);
		boolean would_hit = would_hit_ball(ball);
		ball.transpose(-x_transpose, -y_transpose);
		return would_hit;
	}
	
	public boolean y_within_player_height(double y) {
		return (this.y_pos - this.height) < y && (this.y_pos + this.height) > y;
	}
	
	public void got_hit(){
		if(this.invincibility_time > 0){
			return;
		}
		
		this.invincibility_time = this.invincibility_duration;
		this.exploding = true;
		this.current_img_idx = 0;
		this.lives = Math.max(0, lives-1);
		this.explosion_sound();
	}
	

	public void render(GraphicsContext gc){
		gc.setLineDashes(0);
		gc.setStroke(Color.WHITE);
		gc.setFill(color);
		render_lives(gc);
		if(left){
			gc.fillPolygon(new double[]{x_pos, x_pos-width, x_pos-width}, 
						   new double[]{y_pos, y_pos-height, y_pos+height}, 3);
			gc.strokePolygon(new double[]{x_pos, x_pos-width, x_pos-width}, 
					   new double[]{y_pos, y_pos-height, y_pos+height}, 3);
		}
		else{
			gc.fillPolygon(new double[]{x_pos, x_pos+width, x_pos+width}, 
					   	   new double[]{y_pos, y_pos-height, y_pos+height}, 3);
			gc.strokePolygon(new double[]{x_pos, x_pos+width, x_pos+width}, 
				   	   new double[]{y_pos, y_pos-height, y_pos+height}, 3);
			}
		if(exploding){
			render_explosion(gc);
		}
		for(RotatingBall shield: this.shield_balls) {
			shield.render(gc);
		}
	}
	
	
	public void render_explosion(GraphicsContext gc){
		int y = current_img_idx/8;
		int x = current_img_idx%8;
		int size = 300;
		gc.drawImage(explosion_image, x*100, y*100, 100, 100, x_pos-size/2, y_pos-size/2, size, size);
		
		this.current_img_idx++;
		if(this.current_img_idx == 64){
			this.current_img_idx = 0;
			exploding = false;
		}
	}
	
	
	public void render_lives(GraphicsContext gc){
		double x = 10;
		double y = TriangleShooterView.height - this.live_image.getHeight() -10;
		if(!this.left){
			x = TriangleShooterView.width - this.live_image.getWidth() - 10;
		}
		
		
		for(int i = 0; i<this.lives; i++){
			gc.drawImage(this.live_image, x, y);
			if(left){
				x += this.live_image.getWidth() + 10;
			}
			else{
				x -= this.live_image.getWidth() + 10;
			}
		}
	}
	
	
	public void decrease_cooldown(double time_passed){
		this.current_cooldown -= time_passed;
		this.multishot_time -= time_passed;
		this.invincibility_time -= time_passed;
	}
	
	
	public void go_up(){
		this.y_dir = -speed;
	}
	
	public void go_down(){
		this.y_dir = speed;
	}
	
	public void go_left(){
		this.x_dir = -speed;
	}
	
	public void go_right(){
		this.x_dir = speed;
	}
	
	public double get_center_x(){
		if(left){
			return this.x_pos - width/2;
		}
		return this.x_pos + width/2;
	}
	
	public String toString(){
		return "Player:  x: " + this.x_pos + "   y: " + this.y_pos; 
	}
	
}
