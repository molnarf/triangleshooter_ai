package Game;

import javafx.scene.paint.Color;

public class RotatingBall extends Ball{
	
	private double angle = 270;		// an angle of 0 is to the right of the center
	private double orbit_radius;
	
	public RotatingBall(double x_pos, double y_pos, double radius, double diameter, Color color) {
		super(x_pos, y_pos, 0, 0, diameter, color);
		this.orbit_radius = 80;
	}

	
	/**
	 * 
	 * @param middle_x: The x-position of the rotation center
	 * @param middle_y: The y-position of the rotation center
	 */
	public void move(double middle_x, double middle_y) {
		double orbitSpeed = 5 * C.game_speed;
		double radian = (Math.PI/180) * angle;
		x_pos = middle_x + orbit_radius * Math.cos(radian) - this.width/2;
		y_pos = middle_y + orbit_radius * Math.sin(radian) - this.height/2;
		this.angle += orbitSpeed;
	}
	
}


