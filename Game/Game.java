package Game;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Random;

import PowerUps.BallShield;
import PowerUps.GiantShot;
import PowerUps.MultiShot;
import PowerUps.PowerUp;
import PowerUps.RapidFire;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;

public class Game {
	public Triangle left_player;
	public Triangle right_player;
	public ArrayList<Ball> left_projectiles;	// balls shot by the left player
	public ArrayList<Ball> right_projectiles;
	public ArrayList<PowerUp> power_ups;
	public boolean ingame = true;
	public Triangle winner;
	
	public Game(){
		this.left_projectiles = new ArrayList<>();
		this.right_projectiles = new ArrayList<>();
		this.power_ups = new ArrayList<>();
	}
	

	public Game(boolean init_players) {
		this();
		if(init_players) {
			this.left_player = new Triangle(200, 200, true, Color.LAWNGREEN);
			this.right_player = new Triangle(600, 400, false, Color.RED);
		}
	}
	
	
	public void update(double time_passed){
		for(Ball ball: left_projectiles){
			ball.move();
		}
		for(Ball ball: right_projectiles){
			ball.move();
		}
		remove_balls_outside(left_projectiles);
		remove_balls_outside(right_projectiles);
		
		this.right_player.move();
		this.right_player.decrease_cooldown(time_passed);
		this.right_player.shoot(this.right_projectiles);
		check_collisions(right_player, left_projectiles);

		if(this.left_player != null){
			this.left_player.move();
			this.left_player.decrease_cooldown(time_passed);
			this.left_player.shoot(this.left_projectiles);		
			check_collisions(left_player, right_projectiles);
		}
		// only add powerup here when not training the AI
		if(!TriangleShooterView.train_bots) {
			add_random_powerup();
		}

		
	}
	
	public void remove_balls_outside(ArrayList<Ball> balls) {
		ListIterator<Ball> iter = balls.listIterator();
		while(iter.hasNext()){
			RenderObject projectile = iter.next();
			if(projectile.x_pos < -100 ||
					projectile.y_pos < -100 ||
					projectile.x_pos > TriangleShooterView.width+100 ||
					projectile.y_pos > TriangleShooterView.height+100){
				iter.remove();
			}
		}
	}
	
	
	public void check_collisions(Triangle player, ArrayList<Ball> balls){
		ListIterator<Ball> iter = balls.listIterator();
		while(iter.hasNext()){
			Ball projectile = iter.next();
			if(player.ball_is_in_front_of_player(projectile)){
				Shape boundary = projectile.getBoundary();
				if(player.intersects(boundary)){
					player.got_hit();
					iter.remove();
					continue;
				}
				if(player.shield_intersects(boundary, projectile.color)) {
					iter.remove();
				}
				if(player.lives == 0){
					ingame = false;
					if(player.left){
						winner = right_player;
					}
					else{
						winner = left_player;
					}
				}
			}
		}
		
		ListIterator<PowerUp> iter2 = power_ups.listIterator();
		while(iter2.hasNext()){
			PowerUp p_up = iter2.next();
			if(player.intersects(p_up.getBoundary())){
				p_up.activate(player);
				player.power_ups_collected++;
				iter2.remove();
			}
		}
	}
	

	public void render(GraphicsContext gc){
		this.left_player.render(gc);
		this.right_player.render(gc);
		
		for(PowerUp p_up : power_ups){
			p_up.render(gc);
		}
		for(Ball projectile : left_projectiles){
			projectile.render(gc);
			projectile.render_speed_lines(gc);
		}
		for(Ball projectile : right_projectiles){
			projectile.render(gc);
			projectile.render_speed_lines(gc);
		}
	}
	
	public static PowerUp get_random_powerup() {
		Random rd = new Random();
		double x_pos = rd.nextInt((int) (TriangleShooterView.width - PowerUp.width));
		double y_pos = rd.nextInt((int) (TriangleShooterView.height - PowerUp.height));
		PowerUp p_up;
		int x = rd.nextInt(100);
		if(x < 50){
			p_up = new RapidFire(x_pos, y_pos);
		}
		else if(x < 70) {
			p_up = new BallShield(x_pos, y_pos);
		}
		else if(x < 90){
			p_up = new GiantShot(x_pos, y_pos);
		}
		else{
			p_up = new MultiShot(x_pos, y_pos);
		}
		return p_up;
	}
	
	public void add_powerup(PowerUp p_up) {
		this.power_ups.add(p_up);
	}
	
	public void add_projectile_left(Ball ball){
		this.left_projectiles.add(ball);
	}
	
	public void add_random_projectile_left(){
		Random rd = new Random();
		Ball ball = new Ball(200, rd.nextInt(TriangleShooterView.height), 10, 0);
		this.left_projectiles.add(ball);
	}
	 
	/**
	 * Add a power-up with a certain probability
	 */
	public void add_random_powerup(){
		Random rd = new Random();
		if(rd.nextInt(1000) < 8 * C.game_speed){
			PowerUp p_up = get_random_powerup();
			add_powerup(p_up);
		}
	}
	

	
	public void keyPressed(KeyEvent event){
		KeyCode key = event.getCode();

		if(key == KeyCode.W){
			this.left_player.go_up();
		}
		if(key == KeyCode.A){
			this.left_player.go_left();
		}
		if(key == KeyCode.S){
			this.left_player.go_down();
		}
		if(key == KeyCode.D){
			this.left_player.go_right();
		}
		
		if(key == KeyCode.UP){
			this.right_player.go_up();
		}
		if(key == KeyCode.LEFT){
			this.right_player.go_left();
		}
		if(key == KeyCode.DOWN){
			this.right_player.go_down();
		}
		if(key == KeyCode.RIGHT){
			this.right_player.go_right();
		}
	}
	
	public void keyReleased(KeyEvent event){
		KeyCode key = event.getCode();
		double speed = 0;
		
		if(key == KeyCode.W && this.left_player.y_dir <= speed){
			this.left_player.y_dir = 0;
		}
		if(key == KeyCode.A && this.left_player.x_dir <= speed){
			this.left_player.x_dir = 0;
		}
		if(key == KeyCode.S && this.left_player.y_dir >= speed){
			this.left_player.y_dir = 0;
		}
		if(key == KeyCode.D && this.left_player.x_dir >= speed){
			this.left_player.x_dir = 0;
		}
		
		if(key == KeyCode.UP && this.right_player.y_dir <= speed){
			this.right_player.y_dir = 0;
		}
		if(key == KeyCode.LEFT && this.right_player.x_dir <= speed){
			this.right_player.x_dir = 0;
		}
		if(key == KeyCode.DOWN && this.right_player.y_dir >= speed){
			this.right_player.y_dir = 0;
		}
		if(key == KeyCode.RIGHT && this.right_player.x_dir >= speed){
			this.right_player.x_dir = 0;
		}
	}
	
}
